;
;This file encode must be ANSI
;

[Variables]

; ex.) Shinjuku, Japan
;Latitude=35.689487
;Longtude=139.691711

; ex.)New York
Latitude=40.712776
Longtude=-74.005974

; ex.)Shenzhen
;Latitude=22.5261041
;Longtude=114.0529856

; Forecast and Geocoding common language
Lang=en

;-----------------------------------------
; Labels
UnitsPressure=hPa
UnitsSpeed=m/s
UnitsTemp=��C
UnitsVisibility=km
UnitsHumidity=%
UnitsUVI=
;-----------------------------------------
; for Opencage Reverse geocoding
GeoCodeAPIKey=PASTE-YOUR-OPENCAGE-API-KEY
GeoCodeAPI=https://api.opencagedata.com/geocode/v1/json?q=

; for OpenWeather API
OpenWeatherAPIKey=PASTE-YOUR-OPENWEATHER-API-KEY
OpenWeatherAPI=https://api.openweathermap.org/data/2.5/
OpenWeatherUnits=metric
