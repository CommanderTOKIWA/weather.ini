# weather.ini 6.2 via OpenWeather

One of Rainmeter's skins, Omni.UI's weather.ini, has stopped working due to the removal of Yahoo Weather and wxweather.
In addition, DarkSky was acquired by Apple, which created the need to move to the new API.
![](wx.png)  

So I created a version that uses the OpenWeather API.  
It works as follows.  
![](en.png)


# Requirements

- [Rainmeter](https://www.rainmeter.net/) Latest stable version 
- [Omnimo UI](https://omnimo.info/) version 6+
- [e2e8/Rainmeter-JsonParser](https://github.com/e2e8/Rainmeter-JsonParser) version 1.0

# Files

```
├─WP7                                        // Same structured OmnimoUI
│  └─TextItems
│      └─Weather
│              location.inc                  // localization settings (ANSI)
│              weather.ini                   // main gadget
```
# How it works

## OpenWeather API
Use the OpenWeather One Call API. https://openweathermap.org/api/one-call-api    
Also, OpenWeather can call the API 1,000,000 calls/month for free.  
If you use it with Rainmeter, it is enough.  
This gadget calls the API once every 1 hour, so the total is about 24 times a day.  
(But called when the skin is explicitly reloaded)

## City Name API
Use the OpenCage reverse geocoding API. https://opencagedata.com/api#reverse-resp  
OpenCage can call the API 2500 times a day for free.  
If you use it with Rainmeter, it is enough.  
This gadget calls the API once every 24 hours, so the total is about 1 times a day.  
(But called when the skin is explicitly reloaded)  

# Install and Run

1. Install and run Rainmeter
1. Install Omnimo UI
1. Install e2e8/Rainmeter-JsonParser
    1. Download from [release page](https://github.com/e2e8/rainmeter-jsonparser/releases)
    1. Unzip
    1. `JsonParser.dll` copy into `%USER_PROFILE%\AppData\Roaming\Rainmeter\Plugins`
    1. No need restart Rainmeter
1. Copy my file
    1. WP7 folder copy into your WP7 ex.)`%USERPROFILE%\Documents\Rainmeter\Skins\WP7\`
1. Get a Dark Sky API key (Required)
    1. Access [OpenWeather][APIkey] (Free for 1,000,000 calls/month) complete registration and confirmation
    1. Open `%USERPROFILE%\Documents\Rainmeter\Skins\WP7\TextItems\Weather\location.inc` with text editor
    1. Paste your 32 character API key after `APIKey=`
    1. Save this file
       1. Note: this file must be saved as ANSI, not UTF-8
2. Get a OpenCage API key (Required)
    1. Access [OpenCage][OCkey] (Free for 2500 request per day) complete registration and confirmation
    2. Open `%USERPROFILE%\Documents\Rainmeter\Skins\WP7\TextItems\Weather\location.inc` with text editor
    3. Paste your 32 character API key after `GeoCodeAPIKey=`
    4. Save this file
3. Adjust your location and localization info (Required)
    1. Open `%USERPROFILE%\Documents\Rainmeter\Skins\WP7\TextItems\Weather\location.inc` with text editor.
    2. Get the latitude and longitude information you want to get the weather forecast
    3. There are various ways, but for example https://www.latlong.net/ is easy
    4. Paste the obtained latitude and longitude information after `Latitude=` and `Longtude=` respectively
    5. Save this file
4. Adjust units string reterals (Optional)
    1. Please set arbitrarily according to the following table
    2. Save this file
5. Finaly Load or reloaded weather.ini skin

## [location.inc][inc] parameters
|key            |description                  |ja-JP recommend     |en-US recommend   |
|---------------|-----------------------------|--------------------|------------------|
|Latitude       |Latitude ex.) 999.999        |any                 |any               |
|Longtude       |Longtude ex.) 999.999        |any                 |any               |
|Lang           |Language parameter 4 each APIs |ja                  |en                |
|Units          |[API](https://openweathermap.org/api/one-call-api#data) parameter `units`|si                  |us                |
|UnitsPressure  |Match with your `Units`      |hPa                 |mb                |
|UnitsSpeed     |Same as above                |m/s                 |km/h              |
|UnitsTemp      |Same as above                |°C                  |°F                |
|UnitsVisibility|Same as above                |km                  |km                |
|UnitsHumidity  |Same as above                |%                   |%                 |
|OpenWeatherAPIKey         |Your OpenWeather API Key             |-                   |-                 |
|OpenWeatherAPI     |OpenWeather One Call API URL  |Do not change       |Do not change     |
|GeoCodeAPIKey  |OpenCage API Key             |-                   |-                 |
|GeoCodeAPI     |OpenCage API URL             |Do not change       |Do not change     |

### Notes
- OmnimoUI's `Language` and `CityCode` is not used.

# Links
- Rainmeter https://www.rainmeter.net/
- Omnimo UI https://omnimo.info/
- Omnimo UI on GitHub https://github.com/fediaFedia/Omnimo/
- Json Parser Plugin for Rainmeter https://github.com/e2e8/Rainmeter-JsonParser
- OpenWeather https://openweathermap.org/
- OpenCage GeoAPI https://opencagedata.com/

# Author
- TOKIWA Commander 
- Blogger https://commandertokiwa.blogspot.com/
- Twitter https://twitter.com/CommanderTOKIWA


# License
It is equivalent to it based on Omnimo UI.
- Licensed under [Creative Commons Attribution-NonCommercial-NoDerivs 3.0 License.](https://creativecommons.org/licenses/by-nc-nd/3.0/)

[OCKey]: https://opencagedata.com/
[OCAPI]: https://opencagedata.com/api

[APIkey]: https://openweathermap.org/
[API]: https://openweathermap.org/price
[ini]: WP7/TextItems/Weather/weather.ini
[inc]: WP7/TextItems/Weather/location.inc
[ini2]: WP7/TextItems/Weather/weather-no-city-name.ini


